# README #

Simply move index.php to your web root, and browse to it.
In the index.php file are example uses of the main function, GetServerStatus()

Included also is a API for uTorrent WebUI, which I used in my bookmarks. 
The API is a bitch to track down, so if you ever feel you may need it, keep hold of it.

### What is this repository for? ###

* Create a coloured box with server info including, Name, IP, Port, and a link to access it
* Red box means service down, green means service up.
* Version Release 1.0.0

### Function Call ###

GetServerStatus("IP/Hostname", Port, "Name", Timeout [optional], "Link to Administer" [optional]);

Returns a string containing the outputted box.

### Common Usage ###

echo GetServerStatus('192.168.1.80',80, "MySQLi Server");  
echo GetServerStatus('192.168.1.80',80, "MySQLi Server", 2);  
echo GetServerStatus('192.168.1.80',80, "MySQLi Server", 2, "192.168.1.80/phpmyadmin");  
echo GetServerStatus('192.168.1.1',80, "Router");  
echo GetServerStatus('www.google.com',80, "Google");  
echo GetServerStatus('www.google.com',80, "Google", 5); 

### uTorrent WebUI ###

The uTorrent WebUI is being used in this case to show upload/download speed in the bookmark link like this:

<a href="http://user:password@ip:port/gui/" target="_newtop1">uTorrent <?php echo $UTspeed?></a>  

If you do not use uTorrent, or do not use their web access, then simply remove as required from your code.

### Requirements ###
Web server (Apache, nginx, etc)  
PHP 4+

### Credits ###

* Inspired and originally coded by \_SleepingBag\_ (reddit.com/u/\_SleepingBag\_)
* converted, expanded, and cleaned up by me, (reddit.com/u/DarkJarris)
* uTorrent WebUI API by Miyanokouji http://forum.utorrent.com/viewtopic.php?id=27414&p=1