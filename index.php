<?php
include "/APIs/WebUIfsock.php";

//Set up some variables to avoid Undefined variable notices

//Title bar counter for up/total services
$totalon = 0;
$totaloff = 0;

//uTorrent upload/download speed
$totul = 0;
$totdl = 0;

/**
* Create a box for displaying server status.
* 
* @author DarkJarris
* 
* @version 1.0.2
* 
* @since 1.0.2 Added $name,$admin arguments, returned the entire box instead of pure status.
* @since 1.0.1 Added count for total on/off 
* 
* @param string 	$site //The site (host or IP) to connect to
* @param int 		$port //the port to use
* @param string 	$name //the name to dislay on the box itself
* @param string		$admin //a link to an admin page, default = same as $site:$port
*
* @return string Returns a formatted box
*/
	function GetServerStatus($site, $port, $name, $timeout = 2, $admin = "")
	{
		
		//attempt to connect to the IP
		$fp = @fsockopen($site, $port, $errno, $errstr, $timeout);
		//check if it returned FALSE
		if (!$fp)
			{
				//returned FALSE, server is down/unreachable
				//offline
				global $totaloff;
				$totaloff++;
				$status="background:darkred;color:white;";
			}
		else
			{
				//did NOT return FALSE, server is up.
				//online
				global $totalon;
				$totalon++;
				$status="background:green;color:white;";
			}
		//close the connection now we have our data
		@fclose($fp);
		
		//see if we set an admin link. if so, make a clickable link, otherwise display "None".
		if ($admin  != "")
		{
			$adminlink = "<strong>Access:</strong> <a href=\"$admin\" target=\"_new\">Admin</a>";
		}
		else
		{
			$adminlink = "<strong>Access:</strong> None set";
		}

		//create the box
		$return = "
		<div class=\"square\" style=\"$status\">
			<div style=\"text-align:left;margin:15px;\">
				<center><b><u>$name</u></b></center><br>
				<strong>IP:</strong> {$site}:{$port}<br>
				$adminlink
			</div>
		</div>
		";
		
		return $return;
	}
	
	$utorrent = new uTorrent("ip/hostname", "port", "user", "password");
	$torrents = $utorrent->getTorrents();
	
	if ($utorrent->is_online())
	{
		if (is_array($torrents)) 
		{
			foreach($torrents as $torrent)
			{
				$totul = $totul + $torrent[UTORRENT_TORRENT_UPSPEED];
				$totdl = $totdl + $torrent[UTORRENT_TORRENT_DOWNSPEED];
			}
		}
		$totul = $totul/1024;
		$totdl = $totdl/1024;
		$totdl= sprintf("%01.2f", $totdl);
		$totul= sprintf("%01.2f", $totul);
		$UTspeed = "<span style=\"color:yellow;\">U:$totul kB/s</span> <span style=\"color:lime;\">D:$totdl kB/s</span>";
	}
?>
<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
		<meta http-equiv="refresh" content="30" />
		 
		<style>
		/* -- GLOBALS -- */
		body {  background: #333;  color: #000;  margin: 0px;  font-family: 'Yanone Kaffeesatz', sans-serif;  font-size: 16px;}
		h1 {  font-family: 'Yanone Kaffeesatz', sans-serif;  font-size: 35px;  color: #FAFAFA;  padding: 10px;  background: #000;  width: 400px;}

		a:link {  color:white;  text-decoration: underline;}
		a:visited {  color:white;  text-decoration: underline;}
		a:hover {  color: #222;  text-decoration: none;}
		a:active {  color:white;  text-decoration: underline;}

		/* -- CLASSES -- */
		.square {  width: 175px;  height: 175px;  border: 0px;  display: inline-block;  margin-right: 10px;  margin-top: 10px;}
		.header {  background: #444;  width: 100%;  height: 30px;  min-height: 30px;  margin-left: 0px;  margin-right: 0px;  padding-top: 5px;  color: #888;  border-bottom: 1px solid #666;}
		.header a:link {  color: #CCC;  text-decoration: underline;}
		.header a:visited {  color: #ccc;  text-decoration: underline;}
		.header a:hover {  color: #fff;  text-decoration: none;} 
		.header a:active {  color: #ccc;  text-decoration: underline;}
		</style>
	</head>
	<body>
 		<!-- BEGIN BOOKMARK HEADER -->
		<!-- REMOVE IF YOU DO NOT WANT ACTIVE -->
		<div class="header" align="center">
			<div style="width: 855px;" align="center">
				<a href="http://user:password@ip:port/gui/" target="_newtop1">uTorrent <?php echo $UTspeed?></a> |
				<a href="phpinfo.php" target="_newtop1">PHP Info</a>
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a> |-->
				<!--a href="#" target="_newtop1">Bookmark</a-->
			</div>
		</div>
		<!-- END BOOKMARK HEADER -->
		<!-- REMOVE IF YOU DO NOT WANT ACTIVE -->
		
		<center>
			<div align="center" style="width: 855px;">
				 
				<!-- BEGIN CONTENT -->
				<!-- BEGIN CONTENT -->
				 
				<h1>My Home Network</h1>
				
				<!-- A PORT MUST BE OPEN FOR THIS TO WORK -->
				<!-- MOST COMMON PORTS ARE 22 & 80 -->

				<?php 
					//TEMPLATE
					//Poll the Router
					//echo GetServerStatus('192.168.1.1',80, "Router", 1, "http://192.168.1.1/main.html");
					
					//Poll the Router
					echo GetServerStatus('192.168.1.1',80, "Router", 1, "http://192.168.1.1/main.html");
					
					//Poll google
					echo GetServerStatus('www.google.com',80, "Google", 3);
					
					//Poll the Web server
					echo GetServerStatus('192.168.1.80',80, "Web Server");
				?>

				<!-- END CONTENT -->
				<!-- END CONTENT -->
				 
				<!-- CLEAR FLOATS -->
				<div style="clear: both; min-height: 10px;"></div>
				<!-- CLEAR FLOATS -->
				 
			</div>
		</center>
		<?php $totalcount = $totaloff + $totalon; ?>
		<head>
			<title> <?php echo "($totalon/$totalcount) "; ?>My Home Network</title>
		</head>
	</body>
</html>