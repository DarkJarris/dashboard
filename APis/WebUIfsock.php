<?php
define("UTORRENT_TORRENT_HASH",                0);
define("UTORRENT_TORRENT_STATUS",            1);
define("UTORRENT_TORRENT_NAME",                2);
define("UTORRENT_TORRENT_SIZE",                3);
define("UTORRENT_TORRENT_PROGRESS",            4);
define("UTORRENT_TORRENT_DOWNLOADED",        5);
define("UTORRENT_TORRENT_UPLOADED",            6);
define("UTORRENT_TORRENT_RATIO",            7);
define("UTORRENT_TORRENT_UPSPEED",            8);
define("UTORRENT_TORRENT_DOWNSPEED",            9);
define("UTORRENT_TORRENT_ETA",                10);
define("UTORRENT_TORRENT_LABEL",            11);
define("UTORRENT_TORRENT_PEERS_CONNECTED",    12);
define("UTORRENT_TORRENT_PEERS_SWARM",        13);
define("UTORRENT_TORRENT_SEEDS_CONNECTED",    14);
define("UTORRENT_TORRENT_SEEDS_SWARM",        15);
define("UTORRENT_TORRENT_AVAILABILITY",        16);
define("UTORRENT_TORRENT_QUEUE_POSITION",    17);
define("UTORRENT_TORRENT_REMAINING",            18);
define("UTORRENT_FILEPRIORITY_HIGH",            3);
define("UTORRENT_FILEPRIORITY_NORMAL",        2);
define("UTORRENT_FILEPRIORITY_LOW",            1);
define("UTORRENT_FILEPRIORITY_SKIP",            0);
define("UTORRENT_TYPE_INTEGER",                0);
define("UTORRENT_TYPE_BOOLEAN",                1);
define("UTORRENT_TYPE_STRING",                2);
define("UTORRENT_STATUS_STARTED",            1);
define("UTORRENT_STATUS_CHECKED",            2);
define("UTORRENT_STATUS_START_AFTER_CHECK",    4);

class uTorrent {
    public $sHost;
    public $iPort;
    public $sUser;
    public $sPassword;
    public $iTimeout;
    private $oSocket;

    public function __construct($sHost = '127.0.0.1', $iPort = 32459, $sUser = 'admin', $sPassword = 'utorrent', $iTimeout = 10) {
        $this->sHost = $sHost;
        $this->iPort = $iPort;
        $this->sUser = $sUser;
        $this->sPassword = $sPassword;
        $this->iTimeout = $iTimeout;
    }
    
    private function doRequest($sURL) {
        $this->oSocket = @fsockopen($this->sHost, $this->iPort, $iError, $sError, $this->iTimeout);
        if($this->oSocket) {
            // Connected
            fputs($this->oSocket, 'GET ' . $sURL . '&cid=' . rand(100000000, 259199361) . ' HTTP/1.0' . "\r\n");
            fputs($this->oSocket, 'Authorization: Basic ' . base64_encode($this->sUser . ':' . $this->sPassword) . "\r\n\r\n");
            
            // Retrieve content-length header
            $sHeader = '';
            for(; !strpos($sHeader, "\r\n\r\n"); $sHeader .= fread($this->oSocket, 1));
            $aHeaders = explode("\n", $sHeader);
            $iContentLength = (int) substr($aHeaders[2], 16);
            if($iContentLength) {
                $sRead = fread($this->oSocket, $iContentLength);
                fclose($this->oSocket);
                return $sRead;
            } else {
                // Could not read content length header
                fclose($this->oSocket);
                return 'Could not read content-length';
            }
        } else {
            // Couldn't connect to socket
            return 'Could not connect';
        }
    }

    public function is_online() {
        // Uses start because at present (WebUI 0.31, uTorrent 1.7.2) this simply returns {"":""} and doesn't modify anything
        if($this->doRequest('/gui/?action=')) {
            return true;
        } else {
            return false;
        }
    }

    // Returns an array of all torrent information
    public function getTorrents() {
        $aResponse = json_decode($this->doRequest('/gui/?list=1'), true);
        return $aResponse['torrents'];
    }
    
    // Returns an array of all labels.
    public function getLabels() {
        $aResponse = json_decode($this->doRequest('/gui/?list=1'), true);
        return $aResponse['label'];
    }
    
    private function uploadFile($sFilename, $sHost, $iPort, $sPage, $sPostFieldName, $bGetResponse = false, $bGetHTTPHeaders = false) {
        // Read the file
        $sFileContents = file_get_contents($sFilename);
        
        // Generate a boundary
        $sBoundary = '---------------------------' . substr(sha1(rand(0, 65000) * pi()), 0, 10);
        
        // Set up POST data
        $sData = '--' . $sBoundary . "\n" .
        'Content-Disposition: form-data; name="' . $sPostFieldName . '"; filename="' . basename($sFilename) . '"' . "\n".
        'Content-Type: application/octet-stream' . "\n\n" . $sFileContents . '--' . $sBoundary .
        '--' . "\r\n\r\n";
        
        // Set up POST request
        $sRequest = 'POST ' . $sPage . ' HTTP/1.0' . "\n" .
        'Content-Type: multipart/form-data; boundary=' . $sBoundary . "\n" .
        'Content-Length: ' . strlen($sData) . "\r\n\r\n";
        
        // Open socket
        $this->oSocket = fsockopen($sHost, $iPort);
        
        if($this->oSocket) {
            // Write the request and data to the socket
            fputs($this->oSocket, $sRequest . $sData);
            if($bGetResponse) {
                $sResult = '';
                while(!feof($this->oSocket)) {
                    $sResult .= fread($this->oSocket, 1024);
                }
                fclose($this->oSocket);
                
                // If the user does not want the HTTP headers, strip them
                if(!$bGetHTTPHeaders) {
                    $sResult = substr($sResult, (strpos($sResult, "\r\n\r\n")+4));
                }
                
                return $sResult;
            } else {
                fclose($this->oSocket);
                return true;
            }
        } else {
            // Could not open socket!
            return false;
        }
    }
    
    // If you set $mErrorString then when the function is done and returns false, estring will be a string telling you what error happened.
    // $sFilename could probably be a temporary filenames ($_FILES['somefile']['tmp_name']) but I haven't checked
    // $sFilename can be a URL
    public function addTorrent($sFilename, &$mErrorString = false) { 
        if(substring($sFilename, 0, 7) == 'http://') {
            $this->doRequest('/gui/?action=get-url&s=' . $sFilename);
        } else if(file_exists($sFilename)) {
            $aResponse = json_decode($this->uploadFile($sFilename, $this->sHost, $this->iPort, '/gui/?action=add-file', 'torrent_file', true), true);
            if(isset($aResponse['error'])) {
                if($mErrorString !== false) {
                    $mErrorString = $aResponse['error'];
                }
                return false;
            } else {
                return true;
            }
        } else {
            $mErrorString = 'File does not exist!';
            return false;
        }
    }
    
    private function processHashArgument($mHash) {
        $sHashes = "";
        if(is_array($mHash)) {
            foreach($mHash as $sHash) {
                $sHashes .= "&hash=" . $sHash;
            }
        } else {
            $sHashes = "&hash=" . $mHash;
        }
        
        return $sHashes;
    }
    
    public function removeTorrent($mHash, $bRemoveData = false) {
        if($bRemoveData) {
            $sAction = 'removedata';
        } else {
            $sAction = 'remove';
        }
        
        $this->doRequest('/gui/?action=' . $sAction . $this->processHashArgument($mHash));
    }
    
    public function startTorrent($mHash) {
        $this->doRequest('/gui/?action=start' . $this->processHashArgument($mHash));
    }
    
    public function pauseTorrent($mHash) {
        $this->doRequest('/gui/?action=pause' . $this->processHashArgument($mHash));
    }
    public function stopTorrent($mHash) {
        $this->doRequest('/gui/?action=stop' . $this->processHashArgument($mHash));
    }
    public function forcestartTorrent($mHash) {
        $this->doRequest('/gui/?action=forcestart' . $this->processHashArgument($mHash));
    }
    
    public function recheckTorrent($mHash) {
        $this->doRequest('/gui/?action=recheck' . $this->processHashArgument($mHash));
    }
    
    // This could get the files of multiple torrents, but unfortunately json_decode overwrites the files array each time,
    // instead of doing something nice like having files[i] where i is the number of how many hashes you used. 
    // If someone writes a better JSON decoding function which does that I'd be happy to use that instead.
    public function getFiles($sHash) {
        return json_decode($this->doRequest('/gui/?action=getfiles&hash=' . $sHash), true);
    }
    
    // This could be get the properties of multiple torrents, but unfortunately json_decode overwrites the props array each time,
    // instead of doing something nice like having props[i] where i is the number of how many hashes you used. 
    // If someone writes a better JSON decoding function which does that I'd be happy to use that instead.
    public function getProperties($sHash) {
        return json_decode($this->doRequest('/gui/?action=getprops&hash=' . $sHash), true);
    }
    
    // properties (in format *    propertyname        type and explanation)
    //    trackers        string (what trackers to use seperated by \r\n\r\n)
    //    ulrate            int (maximum upload rate of torrent)
    //    dlrate            int (maximum download rate of torrent)
    //    superseed        bool (whether or not to superseed)
    //    dht            bool (whether or not to use DHT)
    //    pex            bool (whether or not to use peer exchange)
    //    seed_override    bool (whether or not global seeding settings are overridden)
    //    seed_ratio        int (what ratio to seed to before stopping)
    //    seed_time        int (what time to seed to before stopping (seconds))
    //    ulslots            int (number of upload slots)
    
    // This might work using multiple hashes but for now I haven't set to be able to it equally might
    // work with multiple properties and values, again I haven't tried so haven't coded it to do so
    public function setProperty($sHash, $sProperty, $sValue) {
        $this->doRequest('/gui/?action=setprops&hash=' . $sHash . "&s=" . $sProperty . "&v=" . $sValue);
    }
    
    // $file is a number between 0 and the number of files in the torrent minus one. ( I think. might be 1-based)
    // Files are in the order that getFiles returns them in
    // Priority is one of the UTORRENT_FILEPRIORITYs I defined earlier
    // If $mFiles is an array it will set the priority on each
    public function setPriority($sHash, $mFiles, $iPriority) {
        $sFilenumbers = "";
        if(is_array($mFiles)) {
            foreach($mFiles as $iValue) {
                $sFilenumbers .= "&f=" . $iValue;
            }
        } else {
            $sFilenumbers = "&f=" . $mFiles;
        }
        
        $this->doRequest('/gui/?action=setprio&hash=' . $sHash . '&p=' . $iPriority . '&f=' . $sFilenumbers);
    }
    
    public function getSettings() {
        return json_decode($this->doRequest('/gui/?action=getsettings'), true);
    }
    public function setSetting($sSetting, $sValue) {
        $this->doRequest('/gui/?action=setsetting&s=' . $sSetting . "&v=" . $sValue);
    }
}